# FONSim
[![PyPI version](https://badge.fury.io/py/fonsim.svg)](https://badge.fury.io/py/fonsim)
[![Documentation Status](https://readthedocs.org/projects/fonsim/badge/?version=latest)](https://fonsim.readthedocs.io/en/latest/?badge=latest)
![coverage](https://gitlab.com/abaeyens/fonsim/badges/master/coverage.svg)
[![DOI:10.1109/RoboSoft55895.2023.10122049](https://zenodo.org/badge/DOI/10.1109/RoboSoft55895.2023.10122049.svg)](https://doi.org/10.1109/RoboSoft55895.2023.10122049)
#### _Fluidic Object-oriented Network SIMulator_

An object-oriented Python 3 package
designed for simulating pneumatic and hydraulic systems in soft robots.
It is being developed at the [KU Leuven soft robotics group](
  https://softroboticsgroup.com).
Full documentation: [fonsim.org](http://fonsim.org/).

This project is available under the GNU Affero General Public License v3.0 (**agpl-3.0**) license.

### Installation
The pre-packaged release version can be installed using `pip install fonsim`.
See the [PyPI page](https://pypi.org/project/fonsim/) for more information.

Currently, FONSim resides in the beta stage.
Some features do not work well yet, or are unintuitive to use correctly,
and you may encounter bugs.
We look forward to your feedback.


### How to get started
The [examples directory](
  https://gitlab.com/abaeyens/fonsim/-/tree/master/examples)
contains a set of examples showcasing various features of the simulator.
We suggest to start with running the examples.
Furthermore you may want to consult the documentation on
[readthedocs.io](https://fonsim.readthedocs.io/).
The code documentation is also available by the
[Python help function](https://www.programiz.com/python-programming/docstrings#help).
If something is not fully clear, please let us know.

### Key dependencies
* matplotlib
* numpy
* scipy


## Project development, contribution

### Contributing
Are you interested in contributing to this project?
Please get in touch so we can coordinate the development!

### Contributors
Core developers:  
Arne Baeyens  
Bert Van Raemdonck  

As well as many thanks to:  
Edoardo Milana  
Benjamin Gorissen  


## Problems, questions, suggestions
If you have a question the FAQ section does not answer sufficiently,
or you think you have encountered a bug,
you can reach out by creating an issue on the
[GitLab Issues](https://gitlab.com/abaeyens/fonsim/-/issues) page.
The issues page is also the proper place to post suggestions and feedback.


## FAQ
Please refer to the documentation on 
[readthedocs.io](https://fonsim.readthedocs.io/).
