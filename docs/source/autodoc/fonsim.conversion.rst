fonsim.conversion package
=========================

Submodules
----------

fonsim.conversion.indexmatch module
-----------------------------------

.. automodule:: fonsim.conversion.indexmatch
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: fonsim.conversion
   :members:
   :undoc-members:
   :show-inheritance:
