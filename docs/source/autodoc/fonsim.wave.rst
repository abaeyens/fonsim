fonsim.wave package
===================

Submodules
----------

fonsim.wave.custom module
-------------------------

.. automodule:: fonsim.wave.custom
   :members:
   :undoc-members:
   :show-inheritance:

fonsim.wave.wave module
-----------------------

.. automodule:: fonsim.wave.wave
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: fonsim.wave
   :members:
   :undoc-members:
   :show-inheritance:
