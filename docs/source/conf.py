# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html
import warnings
import pkg_resources


# -- Prequel -----------------------------------------------------------------
def get_git_branch():
    """Return string of currently checkout out branch"""
    import os
    import subprocess
    branch = os.environ.get('READTHEDOCS_GIT_IDENTIFIER') \
        if os.environ.get('READTHEDOCS') else \
        subprocess.check_output(
            ['git', 'rev-parse', '--abbrev-ref', 'HEAD']
        ).strip().decode('utf-8')
    if branch is None:
        branch = 'master'
        msg = f"Couldn't determine the current git branch, " + \
              f"hence defaulted to branch 'master'."
        warnings.warn(msg, RuntimeWarning, stacklevel=2)
    return branch


def get_repo_url():
    """URL of the remote repository, used for linking to files"""
    return f'https://gitlab.com/abaeyens/fonsim/-/blob/{get_git_branch()}/'


def get_binder_url():
    """Base URL of the Binder instance"""
    # additional % to escape %2F because later on C-style string format used
    return f'https://mybinder.org/v2/gl/abaeyens%%2Ffonsim/{get_git_branch()}'


def get_version():
    return pkg_resources.require('fonsim')[0].version


# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('../../src/'))


# -- Project information -----------------------------------------------------

project = 'FONSim'
copyright = '2023, FONSim'
author = 'The FONSim Project'

# The full version, including alpha/beta/rc tags
# 'version' should not be empty for EPUB3
release = get_version()
version = get_version()


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.duration',
    #'sphinx.ext.doctest',
    'sphinx.ext.autodoc',
    'sphinx.ext.linkcode',
    #'sphinx.ext.autosummary',
    #'sphinx.ext.intersphinx',
    'sphinx.ext.autosectionlabel',
    'sphinx.ext.extlinks',
    'myst_nb',
    'IPython.sphinxext.ipython_console_highlighting',
    'sphinx_copybutton',
]


def linkcode_resolve(domain, info):
    """sphinx.ext.linkcode"""
    if domain != 'py':
        return None
    if not info['module']:
        return None
    filename = info['module'].replace('.', '/')
    return get_repo_url() + 'src/' + filename + '.py'


intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'sphinx': ('https://www.sphinx-doc.org/en/master/', None),
}
intersphinx_disabled_domains = ['std']

# Make sure the target is unique
autosectionlabel_prefix_document = True

# sphinx.ext.extlinks used to link to files on remote repository
extlinks = {
    'repo': (get_repo_url()+'%s', 'fonsim/%s'),
    'binder': (get_binder_url()+'?labpath=%s', 'fonsim/%s'),
}

# https://docs.readthedocs.io/en/stable/guides/jupyter.html#using-notebooks-in-other-formats
nb_custom_formats = {
    ".md": ["jupytext.reads", {"fmt": "mystnb"}],
}
nb_execution_timeout = 90
nb_execution_show_tb = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', '**.ipynb_checkpoints']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# -- Options for EPUB output
epub_show_urls = 'footnote'

# For making the RTD Flyout Menu expand. Source:
# https://stackoverflow.com/questions/66436744/how-to-activate-the-rtd-flyout-menu
html_js_files = [
    'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js',
    '_static/flyout.js'
]
