============
Introduction
============

This file contains some generic information about the development of FONSim.

Branching
=========
The FONS project branching is based on the
[Driessen or git-flow model](https://nvie.com/posts/a-successful-git-branching-model/).
Put simply,
the ``master`` branch is reserved for production-ready code.
All code in ``master`` should be stable and usable.
The ``dev`` branch contains the latest developed features,
yet as a result the software is not as stable.
The actual features (and improvements in general) are developed in the
feature branches, for example ``feature-plotting``.



To get this repo locally
========================
1. Clone the repo (notice the `$` - this means to do it in a terminal/console).
The directory with the project will be located in the current working directory of the terminal.

::

   $ git clone https://gitlab.com/abaeyens/fonsim.git


1. Go in the created directory (note: one can use `TAB` for autocompletion)

::

   $ cd fonsim


Create a local install
======================
A local install allows to try out the library locally.
This can be useful during development.
First, rename the project root directory to `fonsim` (default name after Git clone: `fons`).
Second, run in the project root directory::

   $ python -m pip install -e .

This installs the FONS package such that it is accessible
like all other Python packages, e.g. using `import fonsim`.
The `-e` option denotes that it uses a symbolic link:
code changes in the project directory (including branch switching)
take effect at the first following `import`.
No re-installation is required.

Note: `python` should refer to Python 3.
You may have to write `python3` to avoid using Python 2.

Note: there appear to be problems with this method on some Windows machines.

Note: if you want to install several versions of the same package on your system,
for example a stable version from PyPi
and a development version from a local install,
you may want to use a
[Python virtual environment](https://docs.python.org/3/tutorial/venv.html).


Development tools
=================
A git repository history visualizer tool like
[gitg](https://wiki.gnome.org/Apps/Gitg/)
can be helpful in developing this software.
It shows the relations between version branches visually,
lists all commits and allows to see the exact changes
were made in a particular commit.
In addition, it can show uncommitted changes.

Gitlab provides similar tools as a web version like the
[GitLab graph](https://gitlab.com/abaeyens/fonsim/-/network/master).


Style guide
===========
https://google.github.io/styleguide/pyguide.html.
