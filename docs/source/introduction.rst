============
Introduction
============
Welcome to the documentation of the FONSim package!

**FONSim** (*Fluidic Object-oriented Network SIMulator*)
is a Python package for simulating fluidic interactions
in lumped fluidic networks.
Both gaseous and liquid fluids are supported,
and FONSim provides a straightforward interface
to define new components using
analytical, simulated and/or measured characteristics.
Furthermore, FONSim comes with a standard library
encompassing the standard pneumatic and hydraulic components
(e.g. pressure sources, major and minor losses)
as well as components popular in soft robotics research
such as inflatable balloons
with nonlinear, experimentally acquired pv-curves.

:binder:`Try FONSim now in your browser, without installing anything,
thanks to Binder<examples/taste_of_fonsim.md>`.
Please note,
the compute resources for the Binder instance are limited
and therefore you can expect a local FONSim installation
to run simulations up to about twenty times faster.

FONSim was introduced at the
6th IEEE-RAS International Conference on Soft Robotics (RoboSoft 2023)
with the publication
`FONS: a Python framework for simulating nonlinear inflatable actuator networks
<https://www.researchgate.net/publication/370796111_FONS_a_Python_framework_for_simulating_nonlinear_inflatable_actuator_networks>`_
[`PDF <_static/FONSim_paper.pdf>`_],
authored by
Arne Baeyens, Bert Van Raemdonck, Edoardo Milana,
Dominiek Reynaerts and Benjamin Gorissen.
FONSim is being developed at the
`KU Leuven soft robotics group <https://softroboticsgroup.com>`_.

It is hoped that this documentation helps you
in exploring FONSim.
If something appears missing: no worries,
please send an email to one of the developers
and they'll kindly help you forward.


How to get started with FONSim
==============================
It is suggested to follow the tutorials in this documentation.
These ought to familiarize the reader with the basic usage of FONSim.
Many users will want to extend FONSim,
as the included standard functionality
is unlikely to fully support their research case.
They will want to continue into the code documentation,
which discusses the five main parts of FONSim.

**Installation**:
FONSim is available on `PyPI <https://pypi.org/project/fonsim/>`_
and therefore can be easily installed with
``pip install fonsim``.
For more detailed intallation instructions,
see :doc:`tutorials/installing`.


Features
========
* Powerful and fast simulation backend

  - Newton-Raphson method for handling nonlinear equations
  - Backward and forward Euler as well as Tustin time discretization
  - Adaptive timestep solver with integrated solution accuracy validation
* Implicit component equations
* Fluid class

  - custom fluids, e.g. non-Newtonian
  - fallback functionality
* Toolset focused on soft robotics (SoRo) research

  - read and process pv-curves
* Standard library of fluidic components

  - Tubes, nodes, pressure sources, volume sources, containers, one-way valves, ...
* Flow calculations

  - Compressible flow approximations
  - Laminar and turbulent flow based on Reynold number
  - Major and minor losses (Darcy, Haaland, K-factor etc.)
*  Preconfigured custom plot methods
* Export data for further processing as JSON file
* Cross platform


Goal and philosophy
===================
The goal of FONSim is to greatly ease simulation of fluidic systems
in softrobotics research
by providing a set of often-needed system components
and analysis tools
and by automating the construction and solving of the resulting component and network equations
with an open-source, easily extensible library.

FONSim is designed in the first place
for usage in research environments,
where flexibility and easy-to-experiment-with software are key.
Therefore, FONSim is fully written in relatively easily-read Python code
such that it is doable to inspect and extend FONSim
without investing too much time and effort.
While this choice for Python (versus, for example, C or C++)
means that FONSim will not excel in solving speed,
this is compensated for by Python being much easier to use.
Finally, the speed difference is not that large,
as for most of the time-consuming numerical maths
the Numpy library is used,
which under the hood relies on highly-optimized C and FORTRAN code.

FONSim does not have a GUI (Graphical User Interface).
While this results in a steeper learning curve in the beginning,
defining systems using code (and not using a GUI)
does have two mayor advantages.
The first advantage is that code is far more powerful that a GUI.
Defining and connecting hundreds of system components
can be done in a few lines using appropriate constructs such as for-loops.
The second advantages is that Python code is easy to collaborate on
and to version manage,
for example using industry-standard tools like Git.

FONSim consists of five parts.
While these parts depend on each other,
they are not finely ingrained to increase flexibility
and ease maintainability.
The first part is the core and is application-agnostic (!).
It is used just as easily to simulate kinematics
as fluidic networks.
Two other parts are the standard components and fluids,
which define most common components used in softrobotics research.
Finally, the three last parts provide some smaller functionality
for data visualization, some data IO
and a function generator.


Contributors and contact
========================
* Core developers:

  - Arne Baeyens
  - Bert Van Raemdonck

* As well as many thanks to:

  - Edoardo Milana
  - Benjamin Gorissen

All the people above can be contacted by email,
the format is ``<first_name>@fonsim.org``.


Citing
======
To cite the FONSim conference publication,
you can use the following bibtex entry:

.. code-block:: bibtex

   @inproceedings{fons2023,
     author={Baeyens, Arne and Van Raemdonck, Bert and Milana, Edoardo
       and Reynaerts, Dominiek and Gorissen, Benjamin},
     booktitle={2023 IEEE International Conference on Soft Robotics (RoboSoft)},
     title={{FONS: a Python framework for simulating nonlinear inflatable actuator networks}},
     year={2023},
     pages={1-6},
     doi={10.1109/RoboSoft55895.2023.10122049},
   }


Contributing
============
Ways to contribute to FONSim
----------------------------
There are several ways to contribute to the FONSim project:
 - Provide feedback about your usage experience.
 - Ask a question.
 - Suggest an improvement, be it to the documentation, the code or anything else.
 - Implement an improvement yourself - please see *Contribution process*.

For all the above, users can email one of the developers
or open a new issue on
`GitLab issues <https://gitlab.com/abaeyens/fonsim/-/issues>`_.

Contribution process
--------------------
1. Check for open issues or open a new issue.
2. Fork the FONSim repository on GitLab.
3. Make your changes.
   Don't forget to add yourself to ``CONTRIBUTORS.txt``!
4. Send a pull request. 
   If no response, email one of the maintainers.

Before submitting the pull request, as a general rule:

- Any code change should come with appropriate tests and documentation.
- All tests should pass.


FAQ
===
**Unconnected terminals appear closed,
they don't bleed into the atmosphere, why?**

Correct, this is expected behaviour because FONSim assumes that
any terminal not connected to another terminal is closed
(i.e. plugged closed with a plug).
If bleeding into the atmosphere is desired,
connect a :py:class:`.Sink`
(or, equivalently,
a :py:class:`.PressureSource` set to atmospheric pressure)
to the terminals in question.


**My simulation does not converge, what should I do?**

A lack of convergence can have several reasons:

- **unphysical system**:
  Some systems are unsolvable because they do not make physical sense,
  for example two :py:class:`.PressureSource` connected together
  without anything imposing a flow resistance inbetween.
  Reasoning about the behaviour of the system and the expected result
  may provide insight.
- **timestep** too large or too small:
  See the :py:class:`.Simulation` documentation on how to specify the timestep,
  and several other simulation parameters.
- **incorrect component residual equations**:
  The component residual equations should specify the component behavior such
  that all solutions for the residual being zero are connected to each other
  in the argument-state space.
  If there are several unconnected solution subspaces, then the solver
  may get stuck in one subspace and be unable to transition to another.
  Reasoning about the value of the residual for several combinations
  of argument and state values may help.
- **incorrect component jacobian**:
  The solver uses the jacobians to determine the direction and magnitude
  of the corrections, and therefore incorrect jacobians can block convergence.
  Try to let FONSim estimate the jacobian by not returning the jacobian
  (only return the residual) in the component equations.
  Or make sure that the component jacobians are correct, for example
  by deriving them with a CAS such as
  `Maxima <https://en.wikipedia.org/wiki/Maxima_(software)>`_.


**My pressure goes negative, why?**

Often, but not always, this is caused by errors in the component equations.
Fluid compressibility can also play a role.
In any case, by default pressures are forced to be positive
starting from FONSim version 0.4.


**Isn't Python too slow for numerical simulations?**

Not necessarily.
While Python's slowness makes it
a poor choice for large simulations that will be run many times,
common soft robotics simulations are small and finish in a few seconds,
and therefore the possible execution speed improvements are limited.
Furthermore, setting up a simulation in Python
takes far less time than doing so in C or C++,
resulting in Python being faster
when considering the overall required duration
from a simulation concept to a numerical result.
Finally, the computationally intensive part critical to scaling
lies in solving the linear system (Newton solver inner loop),
and this part is handled
using the highly-optimized linear solvers in Numpy.

