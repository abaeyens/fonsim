===========
Release log
===========


Upcoming releases
=================
No upcoming release planned soon.


Previous releases
=================
0.4: 2023, May 15
-----------------
A vastly improved solver has made its way into FONSim 0.4.
The automatic timestep adjustment got overhauled completely
and the tradeoff between speed and correctness
is now adjustable to taste by specifying the solving tolerance.
In addition, two new components!

Changes

 - A new solver

   - The timestep adjustment is now based on the interpolation criterium,
     which directly relates the obtained result
     to the desired solving precision, allowing to determine well
     whether the timestep should be decreased or can be increased,
     resulting in faster simulations without sacrificing solution quality.
     The method is explained in :ref:`here<working_principles:Run>`.
     In particular, fast changes (e.g. a square pressure wave)
     are handled far better.
     Be sure to check out the discussion of the new parameters
     of the :py:class:`.Simulation` class.
   - The Newton outer loop convergence criterium now takes into account
     the range of each variable in combination with the specified
     relative solving tolerance.

       For example, if a variable denoting pressure
       ranges from *1.013e5* to *1.213e5* Pa and the relative solving tolerance
       is set to *1e-2*, then the outer loop will run
       until all the equation residuals depending on that pressure variable
       are smaller than *((1.213e5 - 1.013e5) · 1e-2)*
       multiplied by the corresponding sensitivity in the system jacobian.

     For more information: see the documentation of
     :py:meth:`.Simulation.fill_component_equation_tolerances`.
   - The temporal discretization can now be set to backward Euler, forward Euler
     or Tustin. Previously, only the former was supported.
     Tustin discretization gives a smaller phase shift
     at the expense of slightly reduced stability.
   - The first simulation step (index 0) now gets solved as well,
     using forward Euler discretization. Previously, the solution array
     at the first step was left to its initial values because
     solving it required the presence of a preceding step
     (only backward Euler discretization was available).
 - Terminal variable allowed ranges:
   The solver will limits its correction such that the variable
   stays within the given range. The pressure variable in ``terminal_fluidic``
   is now limited to positive values exclusively,
   avoiding convergence to negative pressures.
 - New components:
   :py:class:`.LinearAccumulator` and :py:class:`.CheckValve`.
   The former behaves similarly to a capacitor.
 - Three tutorials are now available as Jupyter notebooks,
   including links to run them on
   `binder <http://mybinder.org>`_.
 - Many documentation improvements.


0.3: 2023, April 03
-------------------
The first beta release!
FONSim appears to work well and no serious issues did arise,
therefore change from alpha to beta.
Extended and cleaned the documentation,
only minor code changes.

Changes
 - Extend and clean documentation.
 - Point to the new [fonsim.org](https://fonsim.org) domain name
   (URL and email addresses).
 - Introduce Binder to try-out FONSim easily.
 - Scipy interpolation bugfix.


0.2a: 2023, February 04
-----------------------
The second release of FONSim
focuses on cleaning up the codebase,
improving documentation to make the codebase
attractive to new users,
and the introduction
of the *autocall* and *autodiff* functionality.

Changes
 - Introduce autocall:
   in component definition,
   reference variables and states by their label,
   removing the requirement to work with arrays and indices.
 - Introduce autodiff:
   in component definition, calculate derivatives
   automatically using numerical differentiation,
   removing the requirement to specify them manually.
 - Introduce more than 100 tests (PyTest framework)
   covering most of FONSim's codebase.
 - Documentation (tutorials + formal codedoc) on readthedocs
   (went online spring 2022).
 - Many bugfixes and code, UI and documentation improvements.
   Some breaking changes were made.


0.1a5: 2021, March 24
---------------------

Initial release of FONSim on PyPI.
