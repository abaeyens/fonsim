=====================
Simulation and solver
=====================

This page provides a rough overview
of the internal workings
of classes :py:class:`.Simulation` and :py:class:`.Solver`.


Call graph ``sim.run()``
========================
The call graph is slightly different
when the simulation step is zero (the first step),
however that case has been omitted for clarity.
Also, the discretization is assumed
to be set to *backward* or *tustin*,
because those are the more complex cases.


With solver :py:class:`.NewtonConstantTimeStep`:

.. code::

   sim.run()
       simstep = 0
       while simulation_not_finished:
       |   solver.run_step(simstep)
       |       <copy arguments from `simstep-1` to `simstep`>
       |       <update time for `simstep`>
       |       solver.newton_solver(simstep)
       |           while newton_not_converged:
       |           |   solver.get_residual(simstep)
       |           |       sim.evaluate_equations(simstep)
       |           |           <load arguments from `simstep`>
       |           |           <load state from simstep-1>
       |           |           <get new state using `component.update_state()`>
       |           |           <get eq residuals using `component.evaluate()`>
       |           |   sim.fill_eq_tol()
       |           |   <solve linear system>
       |           |   <apply correction to step `simstep`>
       |       sim.update_state(simstep)
       |           <get arguments from `simstep`
       |           <get 'previous' state from `simstep-1`>
       |           <get new state using `component.update_state`>
       |           <writeout new state to `simstep`>
       |   sim.fill_xxx_tol()
       |   simstep += 1

TODO
generate it automatically,
for example using https://pycallgraph.readthedocs.io/en/master/.



Brief contents overview of important methods
============================================


Inside method :py:meth:`.Simulation.run()`:

.. code::

   simstep = 0

   while simulation_not_finished:
       solver.run_step(simstep)

       sim.fill_argument_tolerances()
       sim.fill_network_equation_tolerances()
       sim.fill_component_equation_tolerances()

       simstep += 1


Method ``Solver.run_step(simstep)``:

.. code::

   <Copy arguments from `simstep-1` to `simstep`>
   <Update time for `simstep`>
   solver.newton_solver(simstep)
   sim.update_state(simstep, dt)


Method ``Solver.newton_solver(step)``:

.. code::

   while not_converged:
       solver.get_residual(step)
       sim.fill_equation_tolerances()
       <solve linear system>
       <apply correction to step `steþ`>


Method ``Solver.get_residual(simstep)``:

.. code::

  sim.evaluate_equations(simstep)
  <evaluate network equations>


Method ``Simulation.evaluate_equations(simstep)``:

.. code::

  for each component:
      <load arguments from `simstep`>
      <load state from `simstep-1`>
      `component.update_state`
      `componnent.evaluate`
      <update H>


Method ``Simulation.update_state(simstep, dt)``:

.. code::

   for each component:
       <get arguments from `simstep`
       <get 'previous' state from `simstep-1`>
       <get new state using `component.update_state`>
       <writeout new state to `simstep`>
