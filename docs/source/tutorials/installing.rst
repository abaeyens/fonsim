=================
Installation
=================

To install FONSim, follow these four steps:

**Step 0: Installing Python 3**
  FONSim is a Python library and therefore requires a Python interpreter.
  See www.python.org for the appropriate download and installation instructions
  for your operating system.

  Notes
   * On many Linux desktops Python 3 comes preinstalled
     and this step can be safely skipped.
   * To test whether a Python interpreter has been installed,
     open a terminal window and run ``python`` or ``python3``.
     If both return an error messages, there is no Python interpreter installed.

**Step 1: Install PIP**
  TheF ONSim library will be installed using PIP.
  To install PIP, see https://pip.pypa.io/en/stable/installation/.
  To test whether PIP is installed, open a Python shell
  (for example, by starting a command line and running ``python3`` or ``python``)
  and run ``pip``.

**Step 2: Installing FONSim using PIP**
  Installing FONSim is most easily done using PIP.
  This method installs from PyPI, the Python Package Index.
  In terminal, run ``pip install fonsim``.

  For installing FONSim from source, please see the development documentation.


**Step 3: Testing the installation**
  Open a Python shell
  and run, in the Python shell, ``import fonsim``.
  If an error is returned, there is an issue with the installation.


