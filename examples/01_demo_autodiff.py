"""
Example 01
A very simple system

src -=- container

2020, July 22
"""

# Import fonsim package, for building and simulating a fluidic system
import fonsim as fons
# Import matplotlib package, for plotting the simulation results
import matplotlib.pyplot as plt

# A function for the pressure source
waves = [(0, 0.900), (0.50, 0.100)]
wave_function = fons.wave.Custom(waves)*1e5 + fons.pressure_atmospheric

# Create a new pneumatic system
system = fons.System("my_system")

# Choose a fluid
fluid = fons.air

# Create components and add them to the system
# Note: for now, only take major (pipe friction) losses into account.
system.add(fons.PressureSource("source_00", pressure=wave_function))
system.add(fons.Container_autodiff("container_00", fluid=fluid, volume=50e-6))
system.add(fons.CircularTube_autodiff("tube_00", fluid=fluid, diameter=2e-3, length=0.60))

# Connect the components to each other.
system.connect("tube_00", "source_00")
system.connect("tube_00", "container_00")

# Let's simulate the system!
sim = fons.Simulation(system, duration=1, step_min=1e-3, step_max=1e-3)
# Run the simulation
sim.run()


# Plot simulation results
# For matplotlib tutorials, see
# https://matplotlib.org/tutorials/introductory/pyplot.html#sphx-glr-tutorials-introductory-pyplot-py
fig, axs = plt.subplots(4)
fig.suptitle("Simulation results")
axs[0].plot(sim.times, system.get("source_00").get('pressure')*1e-5, label='source_00')
axs[0].plot(sim.times, system.get("container_00").get('pressure')*1e-5, label='container_00')
axs[0].set_ylabel('pressure [bar]')
axs[1].plot(sim.times, system.get("container_00").get_state('mass')*1e3, label='container_00')
axs[1].set_ylabel('mass [g]')
axs[2].plot(sim.times, system.get("source_00").get('massflow')*1e3, label='source_00')
axs[2].plot(sim.times, system.get("container_00").get('massflow')*1e3, label='container_00')
axs[2].set_ylabel('mass flow [g/s]')
axs[3].plot(sim.times, system.get("tube_00").get('pressure', 'a')*1e-5 - system.get("tube_00").get('pressure', 'b')*1e-5, label='tube_00')
axs[3].set_ylabel('delta pressure [bar]')
axs[3].set_xlabel('time [s]')
for a in axs: a.legend()
plt.show()
