"""
Example 02
Based on 01
A slightly more advanced system:
take both major and minor losses into account.

src >-=-< container

2020, July 22
"""

import fonsim as fons
import matplotlib.pyplot as plt

waves = [(0, 0.900), (0.50, 0.100)]
wave_function = fons.wave.Custom(waves)*1e5 + fons.pressure_atmospheric

system = fons.System("my_system")

fluid = fons.air

# Major losses: CircularTube
# Minor losses: FlowRestrictor
tube_diameter = 2e-3
system.add(fons.PressureSource("source_00", pressure=wave_function))
system.add(fons.Container("container_00", fluid=fluid, volume=50e-6))
system.add(fons.CircularTube("tube_00", fluid=fluid, diameter=2e-3, length=0.60))
system.add(fons.FlowRestrictor("restrictor_00", fluid=fluid, diameter=tube_diameter, k=0.5))
system.add(fons.FlowRestrictor("restrictor_01", fluid=fluid, diameter=tube_diameter, k=0.5))

# For the restrictor, we specify the ports, respectively 'a' and 'b'.
system.connect(("restrictor_00",'a'), "source_00")
system.connect("tube_00", ("restrictor_00",'b'))
system.connect(("restrictor_01",'b'), "tube_00")
system.connect("container_00", ("restrictor_01",'a'))

sim = fons.Simulation(system, duration=1)
sim.run()

fig, axs = plt.subplots(4)
fig.suptitle("Simulation results")
axs[0].plot(sim.times, system.get("source_00").get('pressure')*1e-5, label='source_00')
axs[0].plot(sim.times, system.get("container_00").get('pressure')*1e-5, label='container_00')
axs[0].set_ylabel('pressure [bar]')
axs[1].plot(sim.times, system.get("container_00").get_state('mass')*1e3, label='container_00')
axs[1].set_ylabel('mass [g]')
axs[2].plot(sim.times, system.get("source_00").get('massflow')*1e3, label='source_00')
axs[2].plot(sim.times, system.get("container_00").get('massflow')*1e3, label='container_00')
axs[2].set_ylabel('mass flow [g/s]')
axs[3].plot(sim.times, system.get("tube_00").get('pressure', 'a')*1e-5 - system.get("tube_00").get('pressure', 'b')*1e-5, label='major losses')
axs[3].plot(sim.times,
            (system.get("restrictor_00").get('presssure','a')-system.get("restrictor_00").get('presssure','b'))*1e-5
            -\
            (system.get("restrictor_01").get('presssure','a')-system.get("restrictor_01").get('presssure','b'))*1e-5,
            label='minor losses')
axs[3].plot(sim.times,
            system.get("source_00").get('pressure')*1e-5 -  system.get("container_00").get('pressure')*1e-5, label='total losses')
axs[3].set_ylabel('delta pressure [bar]')
axs[3].set_xlabel('time [s]')
for a in axs: a.legend()
plt.show()
