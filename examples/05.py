"""
Example 05
Based on: 04
The wave generator functionality

src -=- act

2020, July 22
"""

import math
import fonsim as fons
import matplotlib.pyplot as plt

# Working with the wave generator: let's generate a square wave
# About lambda expression: https://docs.python.org/3/tutorial/controlflow.html#lambda-expressions
p_min = 0.650e5 + fons.pressure_atmospheric
p_max = 0.890e5 + fons.pressure_atmospheric
wave_function = lambda t: p_min + (p_max-p_min)*fons.wave.square(fons.wave.time_to_angle(t, frequency=1))

system = fons.System("my_system")

fluid = fons.air

system.add(fons.PressureSource("source_00", pressure=wave_function))
system.add(fons.FreeActuator("actu_00", fluid=fluid, curve=None))
system.add(fons.CircularTube('tube_00', fluid=fluid, diameter=2e-3, length=0.60))

system.connect("tube_00", "source_00")
system.connect("tube_00", "actu_00")

sim = fons.Simulation(system, duration=2)
sim.run()

fig, axs = plt.subplots(4)
fig.suptitle("Simulation result")
fons.plot(axs[0], sim, 'pressure', 'bar', ("source_00", "actu_00"))
fons.plot_state(axs[1], sim, 'mass', 'g', ["actu_00"])
fons.plot(axs[2], sim, 'massflow', 'g/s', ["actu_00"])
# Plot the Reynolds number
t = system.get('tube_00')
axs[3].plot(sim.times, 4*abs(t.get('massflow'))/(math.pi*t.d*fluid.mu), label='tube_00')
axs[3].set_ylabel('Re')
axs[3].set_xlabel('time [s]')
for a in axs: a.legend()
plt.show()
