"""
Example of using a CheckValve

A fluidic network consisting of a PressureSource and a Container
connected by a CircularTube and a CheckValve in parallel.
The CheckValve opens when the source pressure is higher
that the container's pressure plus a small threshold,
which makes that the container fills quickly (checkvalve and tube)
yet drains slowly (only tube).

Schematic:
       -=-
src -|     |- container
       ->-

2023, May 07
"""

import fonsim as fons
import matplotlib.pyplot as plt

# Create wave function for pressure source
waves = [(0, 0.0), (0.25, 1.0), (0.5, 0.2), (0.75, 1.0), (1.0, 0.2)]
wave_function = fons.wave.Custom(waves, kind='linear')*1e5\
                + fons.pressure_atmospheric

# Create system
system = fons.System()
# Select fluid
fluid = fons.air
# Create components and add them to the system
system.add(fons.PressureSource('source', pressure=wave_function))
system.add(fons.Container('container', fluid=fluid, volume=100e-6))
system.add(fons.CircularTube('tube', fluid=fluid, diameter=1e-3, length=0.10))
system.add(fons.CheckValve('valve', pressure_threshold=0.1e5))
# Connect components to each other
system.connect(('tube', 'a'), 'source')
system.connect(('tube', 'b'), 'container')
system.connect(('valve', 'a'), 'source')
system.connect(('valve', 'b'), 'container')

# Create simulation and run it
sim = fons.Simulation(system, duration=1.25, step=(2e-3, 1e-1),
                      discretization='backward')
sim.run()

# Plot results
fig, axs = plt.subplots(3, sharex=True)
fig.suptitle("Simulation results")
fons.plot(axs[0], sim, 'pressure', 'bar', ('source', 'container'))
fons.plot_state(axs[1], sim, 'mass', 'g', ('container',))
fons.plot(axs[2], sim, 'massflow', 'g/s', ('container', 'tube', 'valve'))
plt.show()
