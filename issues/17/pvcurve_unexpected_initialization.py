"""
Replicate issue #17
https://gitlab.com/abaeyens/fonsim/-/issues/17

2022, May 22
"""

import fonsim as fons
import matplotlib.pyplot as plt

# Pressure wave
waves = [(0, 0), (1, 0), (2, 2.5), (1, 2.5), (2, 0), (1, 0)]
wave_function = fons.wave.Custom(waves, time_notation='relative', kind='linear'
                                 )*1e5 + fons.pressure_atmospheric

# PV curve
curvefile = 'testPVCurve.csv'
pvcurve = fons.data.PVCurve(data=curvefile,
                            pressure_reference='relative',
                            autocorrect=False,
                            extrapolate=True,
                            extrapolate_derivative=True)

# Define system
system = fons.System("my_system")
fluid = fons.water
system.add(fons.PressureSource("source_00", pressure=wave_function))
# FreeActuator - note the new parameter 'initial_volume'
# set to None to see old behaviour
system.add(fons.FreeActuator("actu_00", fluid=fluid, curve=pvcurve,
                             initial_volume='minimum'))
system.add(fons.CircularTube('tube_00', fluid=fluid, diameter=2e-3, length=2.0))
system.connect("tube_00", "source_00")
system.connect("tube_00", "actu_00")

# Simulate
sim = fons.Simulation(system, duration=wave_function.times[-1], step_min=1e-2, step_max=1e-2)
sim.run()

# Plot simulation results
# Time
fig, axs = plt.subplots(3)
fig.suptitle("Simulation result")
fons.plot(axs[0], sim, 'pressure', 'bar', ("source_00", "actu_00"))
fons.plot_state(axs[1], sim, 'mass', 'g', ["actu_00"])
fons.plot(axs[2], sim, 'massflow', 'g/s', ["tube_00"])
for a in axs: a.legend()
# 2D volume, pressure
v = system.get('actu_00').get_state('mass') * fluid.rho
p = system.get('actu_00').get('pressure') * 1e-5
fig, ax = plt.subplots(1)
ax.plot(v, p)
ax.set_xlabel('volume [ml]')
ax.set_ylabel('absolute pressure [bar]')
plt.show()
