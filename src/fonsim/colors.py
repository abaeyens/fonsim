"""
Color strings
Sources
- http://ozzmaker.com/add-colour-to-text-in-python/
- https://stackoverflow.com/questions/43539956/how-to-stop-the-effect-of-ansi-text-color-code-or-set-text-color-back-to-default
"""

# Reset
off =       '\033[0m'       # Text Reset

# Regular Colors
black =     '\033[0;30m'    # Black
red =       '\033[0;31m'    # Red
green =     '\033[0;32m'    # Green
yellow =    '\033[0;33m'    # Yellow
blue =      '\033[0;34m'    # Blue
purple =    '\033[0;35m'    # Purple
cyan =      '\033[0;36m'    # Cyan
white =     '\033[0;37m'    # White 
