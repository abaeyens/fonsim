"""
Some constants at norm conditions, such as atmospheric pressure
Please refer to the source of this file
to see the defined constants.

2020, July 21
"""

# atmospheric pressure [Pa]
pressure_atmospheric = 1.013 * 10**5

# temperature [K]
temperature = 288.15
