# Component tests
Testing is done by creating a small fluidic system,
running the simulation and verifying the results.
This method is not perfect,
as it would be preferable to test the components on their own,
but it does catches the errors.
