"""
Test components/restrictors.py
2023, May 08
"""

import fonsim as fons
import numpy as np
import math

import pytest
from pytest_parametrize_cases import Case, parametrize_cases

"""
Skip CircularTube because it is already covered by many other tests.
"""


@parametrize_cases(Case(fluid=fons.water, flowrate=100e-3),
                   Case(fluid=fons.air, flowrate=10e-3))
def test_flowrestrictor(fluid, flowrate):
    d, k = 1e-3, 0.5
    sys = fons.System()
    src = fons.MassflowSource('src', massflow=flowrate)
    res = fons.FlowRestrictor('res', fluid, diameter=d, k=k)
    sink = fons.PressureSource('sink', pressure=fons.pressure_atmospheric+0.1e5)
    sys.add(src, res, sink)
    sys.connect(src, res, sink)
    sim = fons.Simulation(sys, duration=2, step=1,
                          relative_solving_tolerance=1e-3)
    sim.run()

    pressure_in = src.get('pressure')[1:]
    pressure_out = fons.pressure_atmospheric + 0.1e5
    massflow = res.get('massflow', 'a')[1:]
    rho = fluid.rho if isinstance(fluid, fons.fluid.IdealIncompressible) \
        else fluid.rho_stp * (pressure_in+pressure_out)*0.5/fons.pressure_atmospheric
    pressure_drop = 8/math.pi**2 * k/(rho*d**4) * massflow**2
    assert pressure_drop == pytest.approx(pressure_in-pressure_out, rel=1e-3)
    assert np.all(pressure_in > fons.pressure_atmospheric)
