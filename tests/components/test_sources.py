"""
Test components/sources.py
2023, May 08
"""

import fonsim as fons
import numpy as np

import pytest
from pytest_parametrize_cases import Case, parametrize_cases

"""
Skip PressureSource because it is already covered by many other tests.
"""


@parametrize_cases(Case(fluid=fons.water, flowrate=200e-3),
                   Case(fluid=fons.air, flowrate=50e-3))
def test_massflowsource(fluid, flowrate):
    sys = fons.System()
    src = fons.MassflowSource('src', massflow=flowrate)
    tube = fons.CircularTube('tube', fluid, diameter=2e-3, length=0.30)
    acc = fons.LinearAccumulator('acc', fluid, k=1e5/100e-6)
    sys.add(src, tube, acc)
    sys.connect(src, tube, acc)
    sim = fons.Simulation(sys, duration=0.1, step=1e-2,
                          relative_solving_tolerance=1e-3)
    sim.run()

    massflow = acc.get('massflow')[1:]
    pressure = tube.get('pressure', 'a')[1:]
    print('presusre', pressure)
    assert massflow == pytest.approx(flowrate, rel=1e-3)
    assert np.all(pressure > fons.pressure_atmospheric)


@parametrize_cases(Case(fluid=fons.water, flowrate=200e-6),
                   Case(fluid=fons.air, flowrate=200e-6))
def test_volumeflowsource(fluid, flowrate):
    sys = fons.System()
    src = fons.VolumeflowSource('src', fluid, volumeflow=flowrate)
    tube = fons.CircularTube('tube', fluid, diameter=2e-3, length=0.30)
    acc = fons.LinearAccumulator('acc', fluid, k=1e5/100e-6)
    sys.add(src, tube, acc)
    sys.connect(src, tube, acc)
    sim = fons.Simulation(sys, duration=0.1, step=1e-2,
                          relative_solving_tolerance=1e-3, discretization='backward')
    sim.run()

    massflow = acc.get('massflow')[1:]
    pressure = tube.get('pressure', 'a')[1:]
    volumeflow = massflow / fluid.rho \
        if isinstance(fluid, fons.fluid.IdealIncompressible) \
        else massflow * fons.pressure_atmospheric / (pressure * fluid.rho_stp)
    assert volumeflow == pytest.approx(flowrate, rel=1e-3)
    assert np.all(pressure > fons.pressure_atmospheric)
