"""
Test components/valves.py
2023, May 07
"""

import fonsim as fons
import numpy as np
import pytest


@pytest.mark.parametrize('pressure_threshold', [0, 0.1e5])
@pytest.mark.parametrize('fluid, diameter',
                         [(fons.water, 3e-3), (fons.air, 1e-3)])
def test_checkvalve(pressure_threshold, fluid, diameter):
    """Test CheckValve
    Network schematic:
           -=-
    src -|     |- acc
           ->-
    """
    timestep = 2e-2

    # Create wave function for pressure source
    waves = [(0, 0.0), (0.25, 1.0), (0.5, 0.2), (0.75, 1.0), (1.0, 0.2)]
    wave_function = fons.wave.Custom(waves, kind='linear')*1e5\
                    + fons.pressure_atmospheric

    # Create system
    sys = fons.System()
    # Create components and add them to the system
    src = fons.PressureSource('src', pressure=wave_function)
    acc = fons.LinearAccumulator('acc', fluid=fluid, k=1e5/100e-6)
    tube = fons.CircularTube('tube', fluid=fluid, diameter=diameter, length=0.10)
    valve = fons.CheckValve('valve', pressure_threshold=pressure_threshold)
    sys.add(src, acc, tube, valve)
    # Connect components to each other
    sys.connect(('tube', 'a'), src)
    sys.connect(('tube', 'b'), acc)
    sys.connect(('valve', 'a'), src)
    sys.connect(('valve', 'b'), acc)

    # Create simulation and run it
    sim = fons.Simulation(sys, duration=1.0, step=timestep,
                          discretization='backward',
                          relative_solving_tolerance=1e-3)
    sim.run()

    if False:
        # Plot results
        import matplotlib.pyplot as plt
        fig, axs = plt.subplots(3, sharex=True)
        fig.suptitle("Simulation results")
        fons.plot(axs[0], sim, 'pressure', 'bar', ('src', 'acc'))
        fons.plot_state(axs[1], sim, 'mass', 'g', ('acc',))
        fons.plot(axs[2], sim, 'massflow', 'g/s', ('acc', 'tube', 'valve'))
        plt.show(block=True)

    # Checks
    pressure_in = tube.get('pressure', 'a')
    pressure_out = tube.get('pressure', 'b')
    pressure_drop = pressure_in - pressure_out
    massflow_valve = valve.get('massflow', 'a')
    # Flow in (check valve open)
    for t in (0.10, 0.72):
        i = int(t/timestep)
        assert pressure_drop[i] == \
               pytest.approx(pressure_threshold, rel=1e-3, abs=1e5*1e-3)
        assert massflow_valve[i] > 0
    # Flow out (check valve closed)
    for t in (0.45, 0.54, 0.92):
        i = int(t/timestep)
        assert np.all(pressure_drop[i] < -pressure_threshold)
        assert massflow_valve[i] == pytest.approx(0, abs=1e-6)
