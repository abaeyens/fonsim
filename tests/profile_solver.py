"""
Example 01
A very simple system

src -=- container

2020, July 22
"""

# Profiling module
import cProfile, pstats, io
from pstats import SortKey


def get_sim():
    # Import fonsim package, for building and simulating a fluidic system
    import fonsim as fons
    # Import matplotlib package, for plotting the simulation results
    import matplotlib.pyplot as plt

    # A function for the pressure source
    waves = [(0, 0.900), (0.50, 0.100)]
    wave_function = fons.wave.Custom(waves)*1e5 + fons.pressure_atmospheric

    # Create a new pneumatic system
    system = fons.System("my_system")

    # Choose a fluid
    fluid = fons.air

    # Create components and add them to the system
    # Note: for now, only take major (pipe friction) losses into account.
    system.add(fons.PressureSource("source_00", pressure=wave_function))
    system.add(fons.Container("container_00", fluid=fluid, volume=50e-6))
    system.add(fons.CircularTube("tube_00", fluid=fluid, diameter=2e-3, length=0.60))

    # Connect the components to each other.
    system.connect("tube_00", "source_00")
    system.connect("tube_00", "container_00")

    # Let's simulate the system!
    sim = fons.Simulation(system, duration=1, step=1e-2)
    # Run the simulation
    #sim.run()

    return sim


sim = get_sim()

pr = cProfile.Profile()
pr.enable()

sim.run()

pr.disable()
s = io.StringIO()
sortby = 'tottime'
ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
ps.print_stats()
print(s.getvalue())
