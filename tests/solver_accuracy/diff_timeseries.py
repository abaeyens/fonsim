"""
Helper function
2022, May 14
"""
import numpy as np


def diff_timeseries(a, at, b, bt):
    """Returns b - a with b resampled to timestamps of a"""
    # Resample b to timestamps of a
    b_r = np.interp(at, bt, b)
    # Take difference and return
    return a - b_r
