import fonsim as fons


def get_system():
    # Create a simple system

    waves = [(0.00, 0.900),
             (0.30, 0.100)]
    wave_function = fons.wave.Custom(waves) * 1e5 + fons.pressure_atmospheric

    system = fons.System("my_system")
    fluid = fons.air
    system.add(fons.PressureSource("source", pressure=wave_function))
    system.add(fons.Container("container", fluid=fluid, volume=50e-6))
    system.add(fons.CircularTube("tube", fluid=fluid, diameter=2e-3, length=0.60))
    system.connect("tube", "source")
    system.connect("tube", "container")

    return system
