

def plot_err(ax, x, y, *args, **kwargs):
    for i in range(len(x)):
        ax.plot([x[i], x[i]], [0, y[i]])
    ax.plot(x, y, *args, **kwargs)
