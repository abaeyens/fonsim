"""
Create and save 'ground truth'
(simulation ran with very high precision)

2022, May 14
"""
import numpy as np

import fonsim as fons
from get_system import get_system

filename = 'ground_truth.npz'

# Create and get system to work with
system = get_system()

# Do very precise simulation (and very slow)
# to get close to ground truth
sim = fons.Simulation(system, duration=0.6, step=1e-4,
                      relative_solving_tolerance=1e-6,
                      minimum_solving_tolerance=1e-15)
sim.run()

# And save these results for later use
np.savez(filename,
         time=sim.times,
         flow=system.get('tube').get('massflow'),
         pressure=system.get('container').get('pressure'),
         )

print('Simulation output saved successfully.')
