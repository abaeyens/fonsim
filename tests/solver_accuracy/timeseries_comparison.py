"""
Tools to compare timeseries

2022, October 15
"""
import numpy as np


def resample_to_shared_x(xa, ya, xb, yb):
    if xa.size == xb.size and (xa == xb).all():
        return xa, ya, yb
    x = np.concatenate((xa, xb))
    x.sort()
    ya = np.interp(x, xa, ya)
    yb = np.interp(x, xb, yb)
    return x, ya, yb


def find_closest(a, v):
    i = np.searchsorted(a, v)
    if i == len(a): return len(a) - 1
    elif i == 0: return 0
    return i - 1 if (v - a[i-1]) < (a[i] - v) else i


assert find_closest([1, 2, 3], 0) == 0
assert find_closest([1, 2, 3], 5) == 2
assert find_closest([1, 2, 3], 1.1) == 0
assert find_closest([1, 2, 3], 1.9) == 1


def isclose_2d(xref, yref, xtest, ytest,
                      x_rtol=10e-2, x_atol=1e-2,
                      y_rtol=1e-6, y_atol=1e-12):
    """
    Extension of function `np.isclose`
    ([numpy documentation](
    https://numpy.org/doc/stable/reference/generated/numpy.isclose.html))
    that handles both y- and x-axis tolerances.

    :param xref: reference x values
    :param yref: reference y values
    :param xtest: x values to test, must be monotonically increasing
    :param ytest: y values to test
    :param x_rtol: relative x tolerance
    :param x_atol: absolute x tolerance
    :param y_rtol: relative y tolerance
    :param y_atol: absolute y tolerance
    :return: True if all within bounds, else False
    """
    assert xref.size == yref.size
    assert xtest.size == ytest.size
    for ir, xr in enumerate(xref):
        it = find_closest(xtest, xr)
        if not np.isclose(ytest[it], yref[it], rtol=y_rtol, atol=y_atol):
            d = max(xr * x_rtol, x_atol)
            j = it
            while j > 0 and xtest[j] > xr - d:
                j -= 1
            passed = False
            ytest_prev = ytest[j]
            while j < len(xtest) and xtest[j] < xr + d:
                if np.isclose(ytest[j], yref[ir], rtol=y_rtol, atol=y_atol) \
                        or ytest_prev < yref[ir] < ytest[j] \
                        or ytest_prev > yref[ir] > ytest[j]:
                    passed = True
                    break
                ytest_prev = ytest[j]
                j += 1
            # Iterating further requires non-existent data
            # => stop comparison
            if j == len(xtest):
                break
            if not passed:
                return False
    return True
