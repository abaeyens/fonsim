"""
Investigate influence of solver timestep
on solving error.

Run ``save_ground_truth.py``
to create file with simulation ground truth.

2022, May 14
"""

import numpy as np
import matplotlib.pyplot as plt

import fonsim as fons
from get_system import get_system
from diff_timeseries import diff_timeseries as diff

filename_groundtruth = 'ground_truth.npz'

# Load ground truth
gt = np.load(filename_groundtruth)
gt_t = gt['time']*1e3
gt_f = gt['flow']*1e3
gt_p = gt['pressure']*1e-5


# Create and get system to work with
def get_simresults(step=1e-2, relative_solving_tolerance=1e-2):
    system = get_system()
    sim = fons.Simulation(system, duration=0.6, step=step,
                          relative_solving_tolerance=relative_solving_tolerance,
                          minimum_solving_tolerance=1e-15)
    sim.run()
    return sim


s01 = get_simresults(1e-2, 1e-6)
s01_t = s01.times*1e3
s01_f = s01.system.get('tube').get('massflow')*1e3
s01_p = s01.system.get('container').get('pressure')*1e-5
s01_fe = diff(gt_f, gt_t, s01_f, s01_t)
s01_pe = diff(gt_p, gt_t, s01_p, s01_t)

s02 = get_simresults((1e-3, 1e-1), 1e-6)
s02_t = s02.times*1e3
s02_f = s02.system.get('tube').get('massflow')*1e3
s02_p = s02.system.get('container').get('pressure')*1e-5
s02_fe = diff(gt_f, gt_t, s02_f, s02_t)
s02_pe = diff(gt_p, gt_t, s02_p, s02_t)


# Plot simulation results
# For matplotlib tutorials, see
# https://matplotlib.org/tutorials/introductory/pyplot.html#sphx-glr-tutorials-introductory-pyplot-py
fig, axs = plt.subplots(4, sharex='col')
fig.suptitle("Simulation results")

axs[0].plot(gt_t, gt_p, '-', label='ground truth')
axs[0].plot(s01_t, s01_p, 's', label='sim 01')
axs[0].plot(s02_t, s02_p, 's', label='sim 02')
axs[0].set_ylabel('pressure [bar]')
axs[1].plot(gt_t, s01_pe, ',', label='sim 01')
axs[1].plot(gt_t, s02_pe, ',', label='sim 02')

axs[2].plot(gt_t, gt_f, '-')
axs[2].plot(s01_t, s01_f, 's')
axs[2].plot(s02_t, s02_f, 's')
axs[2].set_ylabel('massflow [mg/s]')
axs[3].plot(gt_t, s01_fe, ',')
axs[3].plot(gt_t, s02_fe, ',')

axs[3].set_xlabel('time [ms]')
for a in axs: a.legend()
plt.show()
