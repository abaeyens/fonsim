"""
Investigate influence of solver timestep
on solving error.
Look whether it can be checked whether the timestep
is sufficiently small
by using interpolation and comparison.

2022, May 14
"""

import numpy as np
import matplotlib.pyplot as plt

import fonsim as fons
from get_system import get_system
from diff_timeseries import diff_timeseries as diff
from plot_err import plot_err


# Create and get system to work with
def get_simresults(step=1e-2, relative_solving_tolerance=1e-2):
    system = get_system()
    sim = fons.Simulation(system, duration=0.6, step=step,
                          relative_solving_tolerance=relative_solving_tolerance,
                          minimum_solving_tolerance=1e-15)
    sim.run()
    return sim


s = get_simresults(1e-2, 1e-2)
s_t = s.times*1e3
s_f = s.system.get('tube').get('massflow')*1e3
s_p = s.system.get('container').get('pressure')*1e-5
s_fe = diff(s_f, s_t, s_f, s_t)
s_pe = diff(s_p, s_t, s_p, s_t)


# Create new arrays with interpolated values
ip_t = (s_t[:-2] + s_t[2:])/2
ip_f = (s_f[:-2] + s_f[2:])/2
ip_p = (s_p[:-2] + s_p[2:])/2
# and with error
ip_fe = diff(s_f, s_t, ip_f, ip_t)
ip_pe = diff(s_p, s_t, ip_p, ip_t)

# Plot simulation results
# For matplotlib tutorials, see
# https://matplotlib.org/tutorials/introductory/pyplot.html#sphx-glr-tutorials-introductory-pyplot-py
fig, axs = plt.subplots(4, sharex='col')
fig.suptitle("Simulation results")

axs[0].plot(s_t, s_p, '-s', label='ground truth')
axs[0].plot(ip_t, ip_p, 'x', label='sim 01')
axs[0].set_ylabel('pressure [bar]')
plot_err(axs[1], s_t, ip_pe, 'o', label='sim 01')
axs[1].set_ylim(-0.05, 0.05)

axs[2].plot(s_t, s_f, '-s')
axs[2].plot(ip_t, ip_f, 'x')
axs[2].set_ylabel('massflow [mg/s]')
plot_err(axs[3], s_t, ip_fe, 'o')
axs[3].set_ylim(-0.02, 0.02)

axs[3].set_xlabel('time [ms]')
for a in axs: a.legend()
plt.show()
